<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 10:35 AM
 *
 * @package WordPress
 * @subpackage ObservantRecords2020
 * @since Musicwhore 2014 1.0
 */

$promo = (boolean) get_query_var( 'promo' );

if ( $promo === true ) {
    get_template_part( 'single', 'album-promo' );
} else {
    get_template_part( 'single', 'album-detail' );
}