<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 12:21 PM
 *
 * @package WordPress
 * @subpackage Musicwhore 2015
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;
?>

		<div id="footer">
			<div class="container">

				<footer class="row pt-12">
					<nav id="footer-column-1" class="col-md-12 text-center">

						<p>
							&copy <?php echo date('Y'); ?> Observant Records
						</p>

                        <p>
                            <small>This site is protected by reCAPTCHA and the Google
                                <a href="https://policies.google.com/privacy">Privacy Policy</a> and
                                <a href="https://policies.google.com/terms">Terms of Service</a> apply.
                            </small>
                        </p>
					</nav>
				</footer>
			</div>
		</div>

		<?php wp_footer(); ?>

	</body>
</html>