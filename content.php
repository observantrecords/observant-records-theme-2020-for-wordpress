<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 11:05 AM
 *
 * @package WordPress
 * @subpackage Musicwhore 2015
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


    <?php if ( is_search() ): ?>
        <?php the_excerpt(); ?>
    <?php else: ?>
        <?php the_content( __( 'Continue reading &raquo;', WP_TEXT_DOMAIN ) ); ?>
    <?php endif; ?>
	<?php wp_link_pages( array(
		'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'observantrecords2020' ) . '</span>',
		'after'       => '</div>',
		'link_before' => '<span>',
		'link_after'  => '</span>',
	) ); ?>
</article>
