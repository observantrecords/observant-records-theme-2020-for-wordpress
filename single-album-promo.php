<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 10:35 AM
 *
 * @package WordPress
 * @subpackage ObservantRecords2020
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ArtistController;

$artist_meta = get_query_var( 'obrc_artist_meta' );

if ( empty( $artist_meta ) ) {
    $parent_artist = get_post_meta( get_the_ID(), '_ob_artist_parent', true );

    $artist = null;
    if ( !empty( $parent_artist ) ) {
        $parent_api_endpoint = get_post_meta( $parent_artist, '_ob_artist_api_path', true );

        if ( !empty( $parent_api_endpoint ) ) {
            $artist_id = preg_replace('/\/artist\//', '', $parent_api_endpoint );

            if ( !empty( $artist_id ) ) {
                $artist_meta = ArtistController::getArtist( array(
                    'artist' => $artist_id,
                ) );
            }
        }
    }
}

$artist_alias = $artist_meta['alias'];
$cover_url_base = sprintf('%s/artists/%s/albums', TemplateTags::get_cdn_uri(), $artist_alias);

?>
<?php get_header( 'promo' ); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : ?>
    	<?php the_post(); global $post; ?>
        <?php
        $data = [];
        $data['obrc_meta'] = $post->obrc_meta;
        $data['cover_url_base'] = $cover_url_base;
        $data['release_credits'] = get_post_meta( get_the_ID(), '_ob_release_credits', true );
        $data['bandcamp_shortcode'] = get_post_meta( get_the_ID(), '_ob_bandcamp_shortcode', true );
        $data['thumbnail'] = get_the_post_thumbnail_url( get_the_ID() );
        $data['release_post_id'] = get_the_ID();
        $data['artist_alias'] = $artist_alias;
        ?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <?php get_template_part( 'hero', 'promo', $data ); ?>
        </div>
    </div>
</div>
<div class="container">
    <div id="content">
        <div class="row">
            <div class="col-md-12">
                <?php if ( !empty( $post->obrc_meta['primary_release']['ecommerce'] ) ): ?>
                <p>Purchase or listen to <?php echo $post->obrc_meta['title'] ;?> on these services:</p>

                <ul class="list-group pb-3">
                    <?php foreach ( $post->obrc_meta['primary_release']['ecommerce']  as $ecommerce): ?>
                        <li class="list-group-item"><a href="<?php echo $ecommerce['url']; ?>"><?php echo TemplateTags::get_fa_ecommerce_icon( $ecommerce['label'], true ); ?> <?php echo $ecommerce['label']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
                <?php $streaming_items = wp_get_nav_menu_items( 'streaming-menu' ); ?>
                <?php if ( !empty( $streaming_items ) ): ?>
                    <p>Follow <?php echo $artist_meta['display_name']; ?> on these services:</p>

                    <ul class="list-group pb-3">
                        <?php foreach ( $streaming_items as $streaming_item ): ?>
                            <li class="list-group-item"><a href="<?php echo esc_url( $streaming_item->url ) ?>"><span class="<?php echo implode( ' ', $streaming_item->classes ); ?>"></span> <?php echo $streaming_item->post_title; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <?php $social_items = wp_get_nav_menu_items( 'social-menu' ); ?>
                <?php if ( !empty( $social_items ) ): ?>
                    <p>Follow <?php echo $artist_meta['display_name']; ?> on these social media sites:</p>

                    <ul class="list-group pb-3">
                        <?php foreach ( $social_items as $social_item ): ?>
                            <li class="list-group-item"><a href="<?php echo esc_url( $social_item->url ) ?>"><span class="<?php echo implode( ' ', $social_item->classes ); ?>"></span> <?php echo $social_item->post_title; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer( 'promo' );
