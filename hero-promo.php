<?php
namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Release;

$thumbnail = ( isset( $args ) ) ? $args['thumbnail'] : null;
$release = ( isset( $args ) ) ? $args['obrc_meta'] : null;
$artist_alias = ( isset( $args ) ) ? $args['artist_alias'] : null;
?>
<?php if ( !empty( $thumbnail ) ): ?>

<div class="jumbotron hero-header" style="background-image: url( '<?php echo $thumbnail; ?>;">
    <div class="hero-overlay jumbotron-blur">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php TemplateTags::render_cover_image( $release, $artist_alias, 'medium', 'p-3 img-fluid' ); ?>
                <h1><?php echo $release['title']; ?></h1>
                <h2><?php echo $release['artist']; ?></h2>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>
