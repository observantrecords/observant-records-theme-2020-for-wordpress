<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 10:35 AM
 *
 * @package WordPress
 * @subpackage ObservantRecords2020
 * @since Observant Records 2020 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;
?>
<?php get_header(); ?>

	<div class="col-md-12">

        <header>
            <h2>Blog</h2>
        </header><!-- .page-header -->

        <?php get_template_part( 'searchform' ); ?>

        <?php if ( have_posts() ) : ?>
            <div class="row row-cols-1 row-cols-md-3 wp-block-observant-records-news-cards">

			<?php while ( have_posts() ) : // Start the Loop. ?>
				<?php the_post(); ?>
                <?php
                $thumbnail = get_the_post_thumbnail( get_the_ID(), 'small', array(
                    'class' => 'card-img-top'
                ) );
                if ( empty( $thumbnail ) ):
                    $bg_url = get_template_directory_uri() . '/images/blog-index-bg.jpg';
                    $thumbnail = <<< THUMBNAIL
<img src="{$bg_url}" alt="[Observant Records]" />
THUMBNAIL;

                endif;
                ?>
                <div class="col mb-4">
                    <div class="card h-100">
                        <a href="<?php the_permalink(); ?>">
                            <?php echo $thumbnail; ?>
                        </a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a>
                            </h4>
                            <div class="card-text">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endwhile; ?>
            </div>

            <?php TemplateTags::paging_nav(); ?>
		<?php endif; ?>
	</div>

<?php get_footer();
