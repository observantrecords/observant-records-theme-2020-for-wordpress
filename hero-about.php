<?php
namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;

global $post;

$thumbnail = get_the_post_thumbnail_url();
$bg_url = get_template_directory_uri() . '/images/blog-index-bg.jpg';
$srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $post ) );
?>
<?php if ( !empty( $thumbnail ) ): ?>
<div class="jumbotron hero-header" style="background-image: url( <?php echo $bg_url; ?> )">
    <div class="hero-overlay">
        <div class="row">
            <div class="col-md-8">
                <h2 class="display-4"><?php the_title(); ?></h2>

                <?php get_sidebar( 'about' ); ?>
            </div>
            <div class="col-md-4">
                <img src="<?php echo $thumbnail; ?>" class="artist-thumbnail" srcset="<?php echo $srcset; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
            </div>
        </div>
    </div>
</div>

<?php endif; ?>
