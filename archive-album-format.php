<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 10/14/14
 * Time: 7:44 PM
 *
 * @package WordPress
 * @subpackage Musicwhore 2015
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\Eponymous42020;

use ObservantRecords\WordPress\Themes\ObservantRecords2020\TemplateTags;

global $posts;

// Sort posts by release date.
usort( $posts, function ( $a, $b ) {
    if ( !isset( $a->obrc_meta) || !isset( $b->obrc_meta) ) {
        return 0;
    }
    $a_release_date = $a->obrc_meta['primary_release']['release_date'];
    $b_release_date = $b->obrc_meta['primary_release']['release_date'];
    return ( $a_release_date === $b_release_date ) ? 0 :
        ( ( $a_release_date < $b_release_date ) ? 1 : -1 );
} );

// Extract by format.
$albums = array();
$lps = array_filter( $posts, function ( $post ) {
    return ( is_array( $post->obrc_meta ) && $post->obrc_meta['format'] === 'Album' );
} );
if ( !empty( $lps ) ) {
    $albums['Albums'] = $lps;
}
$eps = array_filter( $posts, function ( $post ) {
    return ( is_array( $post->obrc_meta ) && $post->obrc_meta['format'] === 'EP' );
} );
if ( !empty( $eps ) ) {
    $albums['EPs'] = $eps;
}

$singles = array_filter( $posts, function ( $post ) {
    return ( is_array( $post->obrc_meta ) && $post->obrc_meta['format'] === 'Single' );
} );
if ( !empty( $singles ) ) {
    $albums['Singles'] = $singles;
}

$compilations = array_filter( $posts, function ( $post ) {
    return ( is_array( $post->obrc_meta ) && $post->obrc_meta['format'] === 'Compilation' );
} );
if ( !empty( $compilations ) ) {
    $albums['Compilations'] = $compilations;
}

?>
<?php get_header(); ?>

	<div class="col-md-12">

	<?php if ( !empty( $albums ) ) : ?>
		<header>
			<h2>Releases</h2>
		</header>

        <?php foreach ($albums as $format => $releases): ?>

            <h3><?php echo $format; ?></h3>

            <?php $r = 1; ?>
            <?php if ($r % 4 != 0):?>
                <div class="row">
            <?php endif; ?>
            <?php foreach ($releases as $album): ?>
                <div class="col-md-3">
                    <p>
                        <a href="<?php echo get_permalink( $album->ID );  ?>">
                            <?php echo TemplateTags::get_cover_image( $album->obrc_meta, $album->obrc_meta['artist_alias'], 'medium' ); ?>
                        </a>
                    </p>

                    <ul class="list-unstyled">
                        <?php $title = ( !empty( $album->obrc_meta['primary_release']['alternate_title'] ) ) ? $album->obrc_meta['primary_release']['alternate_title'] : $album->obrc_meta['title']; ?>
                        <li><strong><a href="<?php echo get_permalink( $album->ID );  ?>"><?php echo $title; ?></a></strong></li>
                        <?php $album_artist = ( $album->obrc_meta['primary_release']['artist'] != $album->obrc_meta['artist'] ) ? $album->obrc_meta['primary_release']['artist'] : null; ?>
                        <?php if ( !empty( $album_artist ) ): ?>
                            <li><?php echo $album_artist; ?></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <?php if ($r % 4 == 0):?>
                    </div>
                    <div class="row">
                <?php endif; ?>
                <?php $r++; ?>
            <?php endforeach; ?>
            <?php if ( ( $r > 1 && $r-1 % 4 != 0 ) || $r == 1 ):?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>

	<?php endif; ?>
	</div>

<?php get_footer();
