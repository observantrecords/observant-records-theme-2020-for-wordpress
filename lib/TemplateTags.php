<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 10/14/14
 * Time: 9:28 PM
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;

class TemplateTags {
	public static function get_cdn_uri() {
		return OBSERVANTRECORDS_CDN_BASE_URI;
	}

	public static function paging_nav() {
		// Don't print empty markup if there's only one page.
		if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
			return;
		}

		$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
		$pagenum_link = html_entity_decode( get_pagenum_link() );
		$query_args   = array();
		$url_parts    = explode( '?', $pagenum_link );

		if ( isset( $url_parts[1] ) ) {
			wp_parse_str( $url_parts[1], $query_args );
		}

		$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
		$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

		$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
		$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

		// Set up paginated links.
		$pagination_args = array(
			'base'     => $pagenum_link,
			'format'   => $format,
			'total'    => $GLOBALS['wp_query']->max_num_pages,
			'current'  => $paged,
			'mid_size' => 1,
			'add_args' => array_map( 'urlencode', $query_args ),
			'prev_text' => __( '&larr; Previous', 'observantrecords2020' ),
			'next_text' => __( 'Next &rarr;', 'observantrecords2020' ),
			'type' => 'list',
			'list_class' => 'pagination',
            'list_item_class' => 'page-item',
            'list_item_link_class' => 'page-link'
		);
		$links = ( function_exists( 'bootstrap_paginate_links' ) === true ) ? bootstrap_paginate_links( $pagination_args ) : paginate_links( $pagination_args );

		if ( $links ) :

			?>
			<nav role="navigation">
				<h1 class="sr-only"><?php _e( 'Posts navigation', 'observantrecords2020' ); ?></h1>
				<?php echo $links; ?>
			</nav><!-- .navigation -->
		<?php
		endif;
	}

	public static function post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}

		?>
		<nav role="navigation">
			<h4 class="sr-only"><?php _e( 'Post navigation', 'observantrecords2020' ); ?></h4>
			<ul class="pager pagination">
				<?php if ( is_attachment() ) : ?>
					<li class="page-item"><?php previous_post_link( '%link', __( 'Published In %title', 'observantrecords2020' ) ); ?></li>
				<?php else : ?>
					<li class="page-item"><?php previous_post_link( '%link', __( '<span title="Previous Post: %title">Previous</span>', 'observantrecords2020' ) ); ?></li>
					<li class="page-item"><?php next_post_link( '%link', __( '<span title="Next Post: %title">Next</span>', 'observantrecords2020' ) ); ?></li>
				<?php endif; ?>
			</ul>
		</nav><!-- .navigation -->
	<?php
	}

	public static function posted_on() {
		if ( is_sticky() && is_home() && ! is_paged() ) {
			$sticky = translate( 'Sticky', 'observantrecords2020' );
			echo <<< POSTED_ON
<li class="list-inline-item"><span class="fas fa-star"></span> $sticky</li>
POSTED_ON;
		}

		// Set up and print post meta information.
		printf( '
<li class="list-inline-item">
	<span class="fas fa-calendar"></span>
	<a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a>
</li>
<li class="list-inline-item">
	<span class="fas fa-user"></span>
	<a href="%4$s" rel="author">%5$s</a>
</li>',
			esc_url( get_permalink() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			get_the_author()
		);
	}

	public static function get_fa_ecommerce_icon( $label, $return_markup = false ) {

	    $output = null;

	    switch ( $label ) {
            case 'Spotify':
                $icon = 'fa-spotify';
                break;
            case 'iTunes':
                $icon = 'fa-itunes';
                break;
            case 'Amazon MP3':
                $icon = 'fa-amazon';
                break;
            case 'Google Play':
                $icon = 'fa-google-play';
                break;
            case 'Bandcamp':
                $icon = 'fa-bandcamp';
                break;
            default:
                $icon = $label;
        }

        if ( $return_markup === true ) {
            $output = <<< OUTPUT
<span class="fab $icon fa-2xl fa-fw"></span> <span class="sr-only">$label</span>
OUTPUT;
        } else {
            $output = $icon;
        }

        return $output;
    }

    public static function build_cover_image_attributes( $release, $artist_alias, $cover_url_base = '' ) {
        $attributes = array();

        if ( empty( $cover_url_base ) ) {
            $cover_url_base = self::get_cdn_uri();
        }

        if ( !empty( $release ) ) {
            $cover_url_base_path = sprintf( '%s/artists/%s/albums/%s/%s', $cover_url_base, $artist_alias, $release['alias'], strtolower( $release['primary_release']['catalog_num'] ) );
            $attributes['full'] = sprintf( '%s/images/cover_front.jpg', $cover_url_base_path );
            $attributes['large'] = sprintf( '%s/images/cover_front_large.jpg', $cover_url_base_path );
            $attributes['medium'] = sprintf( '%s/images/cover_front_medium.jpg', $cover_url_base_path );
            $attributes['small'] = sprintf( '%s/images/cover_front_small.jpg', $cover_url_base_path );
        }

        return $attributes;
    }

    public static function get_cover_image( $release, $artist_alias, $target_size = 'medium', $class = 'img-album', $cover_url_base = '' ) {
        $output = '';

        $cover = self::build_cover_image_attributes( $release, $artist_alias, $cover_url_base );

        $src = esc_attr( $cover[ $target_size ] );
        $srcset = sprintf( '%s 1425w, %s 713w , %s 356w, %s 178w', $cover['full'], $cover['large'], $cover['medium'], $cover['small'] );

        switch ( $target_size ) {
            case 'small':
                $sizes = '(max-width: 178px) 178px, (min-width: 179px) 178px';
                break;
            case 'medium':
                $sizes = '(max-width: 178px) 178px, (min-width: 179px) 356px';
                break;
            case 'large':
                $sizes = '(max-width: 178px) 178px, (max-width: 767px) 356px, (min-width: 768px) 713px';
                break;
            default:
                $sizes = '(max-width: 178px) 178px, (max-width: 768px) 356px, (max-width: 1424px) 713px, (min-width: 1425px) 1425px';
        }

        $title = $release['title'];

        $output .= <<< OUTPUT
<img src="$src" class="$class" srcset="$srcset" sizes="$sizes" alt="[$title]" title="[$title]" />
OUTPUT;

        return $output;
    }

    public static function render_cover_image( $release, $artist_alias, $target_size = 'medium', $class='img-album', $cover_url_base = '' ) {
        echo self::get_cover_image( $release, $artist_alias, $target_size, $class, $cover_url_base );
    }

}