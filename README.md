# Observant Records 2020 Theme

A custom theme for [Observant Records](https://observantrecords.com/).

## Dependencies

* Gulp

## Implementations

### Bootstrap Overrides for WordPress

`TemplateTags::paging_nav()` checks whether the Bootstrap Overrides plugin is enabled. If so,
it uses `Setup::wp_page_menu_args()` to pass arguments to the overridden paging functions.
See the [plugin documentation](https://bitbucket.org/NemesisVex/bootstrap-overrides-for-wordpress)
for usage.

### Observant Records Artist Connector

The `archive-album`, `content-album`, `content-track` and `sidebar-album` templates query the Observant Records artist database for discography data.

See the [plugin documentation](https://bitbucket.org/observantrecords/observant-records-artist-connector-for-wordpress)
for usage.

### Observant Records Blocks

This plugin provides custom blocks for the Gutenberg editor.

See the [plugin documentation](https://bitbucket.org/observantrecords/observant-records-blocks-for-wordpress)
for usage.

## Child Themes

* [Eponymous 4 2020](https://bitbucket.org/observantrecords/eponymous-4-2020-theme-for-wordpress)
