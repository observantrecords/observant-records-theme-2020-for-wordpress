<?php
namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Release;

$thumbnail = ( isset( $args ) ) ? $args['thumbnail'] : null;
$release = ( isset( $args ) ) ? $args['obrc_meta'] : null;
$bandcamp_shortcode = ( isset( $args ) ) ? $args['bandcamp_shortcode'] : null;
$title = ( !empty( $release['primary_release']['alternate_title'] ) ) ? $release['primary_release']['alternate_title'] : $release['title'];
$parent_artist = get_post_meta( get_the_ID(), '_ob_artist_parent', true );
$artist_permalink = ( !empty( $parent_artist ) ) ? get_permalink( $parent_artist ) : null;
$primary_release_artist = ( $release['primary_release']['artist'] != $release['artist'] ) ? $release['primary_release']['artist'] : $release['artist'];
$release_artist = ( !empty( $artist_permalink ) ) ? sprintf( '<a href="%s" class="release-artist-site">%s</a>', $artist_permalink, $primary_release_artist) : $primary_release_artist;

?>
<?php if ( !empty( $thumbnail ) ): ?>
    <?php
    if ( !empty ( $release['primary_release']['ecommerce'] ) ):
        if ( count( $release['primary_release']['ecommerce']  ) > 0):
            foreach ( $release['primary_release']['ecommerce']  as $ecommerce):
                if ( $ecommerce['label'] == 'Bandcamp' ):
                    $ecommerce_buy_now = $ecommerce;
                else:
                    $ecommerce_also_available[] = $ecommerce;
                endif;
            endforeach;
        endif;
    endif;
    ?>

<div class="jumbotron hero-header" style="background-image: url( '<?php echo $thumbnail; ?>' )">
    <div class="hero-overlay">
        <div class="row">
            <div class="col-md-6">
                <h5>Release</h5>
                <h2 class="display-4"><?php echo $title; ?></h2>
                <h3><?php echo $release_artist; ?></h3>
                <?php if ( !empty( $ecommerce_buy_now ) ): ?>
                    <p>
                        <a class="btn btn-primary btn-lg mb-2 mt-2" href="<?php echo $ecommerce_buy_now['url']; ?>" role="button">Buy</a>
                    </p>
                <?php endif; ?>
                <?php if ( !empty( $ecommerce_also_available ) ): ?>
                <p>
                    Also available from:
                </p>
                <ul class="list-inline hero-ecommerce-list">
                    <?php foreach ( $ecommerce_also_available as $ecommerce ): ?>
                        <li class="list-inline-item"><a href="<?php echo $ecommerce['url']; ?>"><?php echo TemplateTags::get_fa_ecommerce_icon( $ecommerce['label'], true ); ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php echo do_shortcode( $bandcamp_shortcode ); ?>
            </div>
        </div>
    </div>

</div>

<?php endif; ?>
