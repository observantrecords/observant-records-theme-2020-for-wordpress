<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 11:05 AM
 *
 * @package WordPress
 * @subpackage Musicwhore 2015
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ArtistController;

$thumbnail = get_the_post_thumbnail_url( );


$artist_taxonomy_link = get_term_link( $post->post_name, 'artists' );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header>
		<?php if ( is_single() ): ?>
            <?php if ( empty( $thumbnail ) ): ?>
                <?php echo the_title('<h2 class="entry-title">', '</h2>'); ?>
            <?php endif; ?>
		<?php else: ?>
			<?php echo the_title('<h3 class="entry-title"><a href="' . esc_url( get_permalink() )  . '" rel="bookmark">', '</a></h3>'); ?>
		<?php endif; ?>

		<div class="entry-meta">
			<ul class="list-inline">
				<?php edit_post_link( __( 'Edit', WP_TEXT_DOMAIN ), '<li class="list-inline-item"><span class="fas fa-pencil-alt"></span> ', '</li>' ); ?>
			</ul>
		</div>

	</header>

	<?php the_content( __( 'Continue reading &raquo;', WP_TEXT_DOMAIN ) ); ?>
</article>
