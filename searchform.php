<?php
namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;
?>
<form class="add-bottom-spacer" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
		<label for="s">
			<span class="sr-only">Search for:</span>
		</label>
        <input type="hidden" name="post_type" value="post" />
		<input type="search" class="form-control" placeholder="Search …" value="<?php echo get_search_query(); ?>" name="s" title="Search for:" />
	</div>
	<input type="submit" class="search-submit btn btn-secondary" value="Search" />
</form>
