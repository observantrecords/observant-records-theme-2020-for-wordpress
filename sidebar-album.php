<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 10/14/14
 * Time: 5:07 PM
 *
 * @package WordPress
 * @subpackage ObservantRecords2020
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;

$thumbnail = ( isset( $args ) ) ? $args['thumbnail'] : null;
$release = ( isset( $args ) ) ? $args['obrc_meta'] : null;
$release_credits = ( isset( $args ) ) ? $args['release_credits'] : null;
$release_artist_alias = ( !empty( $release['artist_alias'] ) ) ? $release['artist_alias'] : null;
$default_artist_alias = ( isset( $args ) ) ? $args['artist_alias'] : null;
$artist_alias = ( !empty( $release_artist_alias ) ) ? $release_artist_alias : $default_artist_alias;

?>

<?php if ( empty( $thumbnail ) ): ?>
    <?php

    $ecommerce_buy_now = null;
    $ecommerce_also_available = [];

    if ( !empty ($release ) ):
        if ( count( $release['primary_release']['ecommerce'] ) > 0 ):
            foreach ( $release['primary_release']['ecommerce'] as $ecommerce):
                if ( $ecommerce['label'] == 'Bandcamp' ):
                    $ecommerce_buy_now = $ecommerce;
                else:
                    $ecommerce_also_available[] = $ecommerce;
                endif;
            endforeach;
        endif;
        ?>
    <div class="release-shop">
        <h3>Buy now</h3>

        <?php if ( !empty( $ecommerce_buy_now ) ): ?>
            <ul class="list-inline">
                <li><a href="<?php echo $ecommerce_buy_now->ecommerce_url; ?>" class="btn btn-primary btn-lg"><span class="fab fa-bandcamp fa-xl fa-fw"></span> Bandcamp</a></li>
            </ul>
        <?php endif; ?>

        <?php if ( count($ecommerce_also_available) > 0): ?>
            <h4>Also available from:</h4>

            <ul>
                <?php foreach ( $ecommerce_also_available as $ecommerce ): ?>
                    <?php if ( $ecommerce['label'] != 'Observant Records Shop' && $ecommerce['label'] != 'Bandcamp' ): ?>
                        <li><a href="<?php echo $ecommerce['url']; ?>"><?php echo $ecommerce['label']; ?></a></li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
    <?php endif; ?>
<?php endif; ?>

    <div class="release-info">
        <h3>Release info</h3>

        <div class="row release-info-container">
            <div class="col-md-3 release-info-column">
                <?php if ( !empty( $release ) ): ?>
                    <p>
                        <?php TemplateTags::render_cover_image( $release, $artist_alias, 'medium', 'img-fluid' ); ?>
                    </p>
                    <?php if ( !empty( $release['release_date'] ) ): ?>
                        <ul class="list-unstyled">
                            <li>Release date: <strong><?php echo date('F d, Y', strtotime( $release['release_date'] ) ); ?></strong></li>
                            <li>Catalog no.: <strong><?php echo $release['primary_release']['catalog_num']; ?></strong></li>
                        </ul>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="col-md-9 release-info-column">
                <?php if ( !empty( $release_credits ) ): ?>
                    <?php echo $release_credits; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
