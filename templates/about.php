<?php
/*
Template Name: About Page
 */

/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 10:35 AM
 *
 * @package WordPress
 * @subpackage Musicwhore2015
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\Eponymous42020;

?>
<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        <div class="col-md-12">
            <header>
                <?php get_template_part( 'hero', 'about' ); ?>
            </header>
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php get_template_part( 'content', get_post_format() ); ?>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>



<?php get_sidebar(); ?>
<?php get_footer();
