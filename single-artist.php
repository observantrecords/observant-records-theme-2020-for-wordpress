<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 10:35 AM
 *
 * @package WordPress
 * @subpackage ObservantRecords2020
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;
use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ArtistController;

$artist_slug = $post->post_name;
$album_posts = new \WP_Query([
    'post_type' => 'album',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'tax_query' => [
        [
            'taxonomy' => 'artists',
            'field' => 'slug',
            'terms' => $artist_slug,
        ],
    ],
]);

$blog_posts = new \WP_Query([
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 3,
    'tax_query' => [
        [
            'taxonomy' => 'artists',
            'field' => 'slug',
            'terms' => $artist_slug,
        ],
    ],
]);

if ( !empty( $album_posts->found_posts > 0 ) ) {
    usort( $album_posts->posts, function ( $a, $b ) {
        if ( !isset( $a->obrc_meta) || !isset( $b->obrc_meta) ) {
            return false;
        }
        $a_release_date = $a->obrc_meta['primary_release']['release_date'];
        $b_release_date = $b->obrc_meta['primary_release']['release_date'];
        return ( $a_release_date === $b_release_date ) ? 0 :
            ( ( $a_release_date < $b_release_date ) ? 1 : -1 );
    } );
}

get_header();
?>
	<?php if ( have_posts() ) : ?>
		<?php  while ( have_posts() ) : ?>
            <?php the_post(); ?>
            <div class="col-md-12">
                <header>
                    <?php get_template_part( 'hero', 'artist' ); ?>
                </header>

                <div class="row">
                    <div class="col-lg-10 offset-lg-1 artist-about-content">
                        <?php get_template_part( 'content', 'artist' ); ?>
                    </div>
                </div>

                <?php if ( $blog_posts->found_posts > 0 ): ?>
                <div class="row">
                    <div class="col-md-12">
                        <h2>Latest news</h2>

                        <div class="row row-cols-1 row-cols-md-3 wp-block-observant-records-news-cards">
                            <?php while ( $blog_posts->have_posts() ): ?>
                                <?php $blog_posts->the_post(); ?>
                                <?php
                                $thumbnail = get_the_post_thumbnail( get_the_ID(), 'small', array(
                                    'class' => 'card-img-top'
                                ) );
                                if ( empty( $thumbnail ) ):
                                    $bg_url = get_template_directory_uri() . '/images/blog-index-bg.jpg';
                                    $thumbnail = sprintf( '<img src="%s" alt="[Observant Records]" />', $bg_url );
                                endif;
                                ?>
                                <div class="col mb-4">
                                    <div class="card h-100">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php echo $thumbnail; ?>
                                        </a>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </h4>
                                            <div class="card-text">
                                                <?php echo get_the_excerpt(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>

                        <?php if ( $blog_posts->max_num_pages > 1 ): ?>
                            <p>
                                <a href="<?php echo get_term_link( $artist_slug, 'artists' ); ?>">Read more &raquo;</a>
                            </p>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endif; ?>

                <?php if ( $album_posts->found_posts > 0 ): ?>
                <div class="row">
                    <div class="col-md-12">
                        <h2>Releases</h2>

                        <?php $r = 1; ?>
                        <div class="row">
                            <?php foreach ( $album_posts->posts as $album ): ?>
                            <?php if ( isset( $album->obrc_meta) ): ?>
                            <?php
                            $artist = null;
                            if ( empty( $album->obrc_meta['artist_meta'] ) ) {
                                $parent_artist = get_post_meta( $album->ID, '_ob_artist_parent', true );
                                if ( !empty( $parent_artist ) ) {
                                    $parent_api_endpoint = get_post_meta( $parent_artist, '_ob_artist_api_path', true );

                                    if ( !empty( $parent_api_endpoint ) ) {
                                        $artist = ArtistController::getArtistByPath( $parent_api_endpoint );
                                    }
                                }
                            } else {
                                $artist = $album->obrc_meta['artist_meta'];
                            }

                            ?>
                            <?php if ($r % 4 == 0):?>
                            <?php endif; ?>
                            <div class="col-md-3">
                                <?php if ( !empty( $artist ) ): ?>
                                    <?php $cover_url_base = sprintf('%s/artists/%s/albums', TemplateTags::get_cdn_uri(), $artist['alias']); ?>
                                    <?php $cover_url = sprintf('%s/%s/%s/images/cover_front_medium.jpg', $cover_url_base, $album->obrc_meta['alias'], strtolower( $album->obrc_meta['primary_release']['catalog_num'] ) ); ?>
                                    <p>
                                        <a href="<?php echo get_permalink( $album->ID );  ?>">
                                            <img src="<?php echo $cover_url; ?>" width="100%" alt="<?php echo $album->obrc_meta['title']; ?>" title="<?php echo $album->obrc_meta['title']; ?>" />
                                        </a>
                                    </p>
                                <?php endif; ?>

                                <ul class="list-unstyled">
                                    <?php $title = ( !empty( $album->obrc_meta['primary_release']['alternate_title'] ) ) ? $album->obrc_meta['primary_release']['alternate_title'] : $album->obrc_meta['title']; ?>
                                    <li><strong><a href="<?php echo get_permalink( $album->ID );  ?>"><?php echo $title; ?></a></strong></li>
                                    <?php $album_artist = ( $album->obrc_meta['primary_release']['artist'] != $album->obrc_meta['artist'] ) ? $album->obrc_meta['primary_release']['artist'] : null; ?>
                                    <li><?php echo $album_artist; ?></li>
                                </ul>
                            </div>
                            <?php if ($r % 4 == 0):?>
                        </div>
                        <div class="row">
                            <?php endif; ?>
                            <?php $r++; ?>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>


        <?php endwhile; ?>
	<?php endif; ?>
<?php  get_footer();
