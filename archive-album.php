<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 10/14/14
 * Time: 7:44 PM
 *
 * @package WordPress
 * @subpackage Musicwhore 2015
 * @since Musicwhore 2014 1.0
 */

if ( is_child_theme() ) {
    get_template_part( 'archive', 'album-format' );
} else {
    get_template_part( 'archive', 'album-all' );
}