<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 10/14/2014
 * Time: 12:21 PM
 *
 * @package WordPress
 * @subpackage Musicwhore 2015
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;
?>
				</div>
			</div>
		</div>

        <div id="newsletter-cta">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col col-lg-8 p-4">
                        <h3>Join the Newsletter</h3>

                        <p>
                            Get the news about the latest releases and events, including subscriber exclusives.
                        </p>

                        <?php echo do_shortcode( '[mailchimp size=inline]' ); ?>
                    </div>
                </div>
            </div>
        </div>

		<div id="footer">
			<div class="container">

				<footer class="row pt-4">
					<nav id="footer-column-1" class="col-md-4">
                        <?php $streaming_items = wp_get_nav_menu_items( 'streaming-menu' ); ?>
                        <?php $social_items = wp_get_nav_menu_items( 'social-menu' ); ?>

                        <?php if ( !empty( $streaming_items ) || !empty( $social_items ) ): ?>
                            <h3>Connect</h3>
                        <?php endif; ?>

                        <?php if ( !empty( $streaming_items ) ): ?>
                            <ul id="nav-buy">
                                <?php foreach ( $streaming_items as $streaming_item ): ?>
                                    <li><a href="<?php echo esc_url( $streaming_item->url ) ?>"><span class="<?php echo implode( ' ', $streaming_item->classes ); ?>"></span> <span class="sr-only"><?php echo $streaming_item->post_title; ?></span></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <?php if ( !empty( $social_items ) ): ?>
                            <ul id="nav-social">
                                <?php foreach ( $social_items as $social_item ): ?>
                                    <li><a href="<?php echo esc_url( $social_item->url ) ?>"><span class="<?php echo implode( ' ', $social_item->classes ); ?>"></span> <span class="sr-only"><?php echo $social_item->post_title; ?></span></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
					</nav>

					<section id="footer-column-2" class="col-md-4">
						<h3>Artists</h3>

						<?php $artist_menu_args = array( 'theme_location' => 'footer-artists', 'items_wrap' => '<ul id="%1$s" class="%2$s links">%3$s</ul>' ); ?>
						<?php if ( function_exists( 'bootstrap_page_menu' ) ) { $artist_menu_args[ 'fallback_cb' ] = 'bootstrap_page_menu'; } ?>
						<?php wp_nav_menu( $artist_menu_args ); ?>
					</section>

					<section id="footer-column-3" class="col-md-4">
						<h3>More Information</h3>

						<?php $info_menu_args = array( 'theme_location' => 'footer-info', 'items_wrap' => '<ul id="%1$s" class="%2$s links">%3$s</ul>' ); ?>
						<?php if ( function_exists( 'bootstrap_page_menu' ) ) { $info_menu_args[ 'fallback_cb' ] = 'bootstrap_page_menu'; } ?>
						<?php wp_nav_menu( $info_menu_args ); ?>

                        <p>
                            &copy <?php echo date('Y'); ?> Observant Records
                        </p>
					</section>
				</footer>
			</div>
		</div>

		<?php wp_footer(); ?>

	</body>
</html>