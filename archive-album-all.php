<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 10/14/14
 * Time: 7:44 PM
 *
 * @package WordPress
 * @subpackage Musicwhore 2015
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Controllers\Api\V2\ArtistController;

global $posts;

// Sort posts by release date.
usort( $posts, function ( $a, $b ) {
    if ( !isset( $a->obrc_meta) || !isset( $b->obrc_meta) ) {
        return false;
    }
    $a_release_date = $a->obrc_meta['primary_release']['release_date'];
    $b_release_date = $b->obrc_meta['primary_release']['release_date'];
    return ( $a_release_date === $b_release_date ) ? 0 :
        ( ( $a_release_date < $b_release_date ) ? 1 : -1 );
} );

$albums = array_filter( $posts, function ( $post ) {
    return ( is_array( $post->obrc_meta ) && $post->obrc_meta['visible'] == true );
});

?>
<?php get_header(); ?>

	<div class="col-md-12">
    <?php if ( !empty( $albums ) ) : ?>
		<header>
			<h2>Releases</h2>
		</header>

        <?php $r = 1; ?>
        <div class="row">
        <?php foreach ( $albums as $album ): ?>
            <?php
            $artist = null;
            if ( empty( $album->obrc_meta['artist_meta'] ) ) {
                $parent_artist = get_post_meta( $album->ID, '_ob_artist_parent', true );
                if ( !empty( $parent_artist ) ) {
                    $parent_api_endpoint = get_post_meta( $parent_artist, '_ob_artist_api_path', true );

                    if ( !empty( $parent_api_endpoint ) ) {
                        $artist = ArtistController::getArtistByPath( $parent_api_endpoint );
                    }
                }
            } else {
                $artist = $album->obrc_meta['artist_meta'];
            }
            $title = ( !empty( $album->obrc_meta['primary_release']['alternate_title'] ) ) ? $album->obrc_meta['primary_release']['alternate_title'] : $album->obrc_meta['title'];
            $album_artist = ( !empty( $album->obrc_meta['primary_release']['artist'] ) ) ? $album->obrc_meta['primary_release']['artist'] : $album->obrc_meta['artist'];

            ?>
            <?php if ($r % 4 == 0):?>
            <?php endif; ?>
            <div class="col-md-3">

                <?php if ( !empty( $artist ) ): ?>
                    <p>
                        <a href="<?php echo get_permalink( $album->ID );  ?>">
                            <?php echo TemplateTags::get_cover_image( $album->obrc_meta, $album->obrc_meta['artist_alias'], 'medium' ); ?>
                        </a>
                    </p>
                <?php endif; ?>

                <ul class="list-unstyled">
                    <li><strong><a href="<?php echo get_permalink( $album->ID );  ?>"><?php echo $title; ?></a></strong></li>
                    <li><?php echo $album_artist; ?></li>
                </ul>
            </div>
            <?php if ($r % 4 == 0):?>
                </div>
                <div class="row">
            <?php endif; ?>
            <?php $r++; ?>
        <?php endforeach; ?>
        </div>
	<?php endif; ?>
	</div>

<?php get_footer();
