<?php
namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;

global $post;

$thumbnail = get_the_post_thumbnail_url();
$bg_url = get_template_directory_uri() . '/images/blog-index-bg.jpg';
$srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $post ) );
$social_menu_items = wp_get_nav_menu_items( sprintf( '%s-%s', 'social-menu', $post->post_name ) );
?>
<?php if ( !empty( $thumbnail ) ): ?>
<div class="jumbotron hero-header" style="background-image: url( <?php echo $bg_url; ?> )">
    <div class="hero-overlay">
        <div class="row">
            <div class="col-md-8">
                <h5>Artist</h5>
                <h2 class="display-4"><?php the_title(); ?></h2>
                <?php if ( !empty( $post->obrc_meta['url'] ) ): ?>
                    <ul class="list-inline">
                        <a href="<?php echo $post->obrc_meta['url']; ?>">Artist website</a>
                    </ul>
                <?php endif; ?>
                <?php if ( false !== $social_menu_items ): ?>
                    <ul class="list-inline hero-ecommerce-list">
                        <?php foreach ( $social_menu_items as $social_menu_item ): ?>
                            <li class="list-inline-item">
                                <a href="<?php echo $social_menu_item->url; ?>" title="<?php echo $social_menu_item->post_title; ?>">
                                    <span class="<?php echo implode( ' ', $social_menu_item->classes ); ?>"></span>
                                    <span class="sr-only"><?php echo $social_menu_item->post_title; ?></span></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

            </div>
            <div class="col-md-4">
                <img src="<?php echo $thumbnail; ?>" class="artist-thumbnail" srcset="<?php echo $srcset; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
            </div>
        </div>
    </div>
</div>

<?php endif; ?>
