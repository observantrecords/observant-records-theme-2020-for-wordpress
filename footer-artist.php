<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 11/9/14
 * Time: 10:44 AM
 */

namespace ObservantRecords\WordPress\Themes\Eponymous42020;

use \ObservantRecords\WordPress\Themes\ObservantRecords2020\TemplateTags;

?>

            </div>
		</div>
	</div>

    <?php $hide_cta = boolval( get_theme_mod( 'hide_cta' ) ); ?>
    <?php if ( $hide_cta === false ): ?>
    <div id="newsletter-cta">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-lg-8 p-4">
                    <h3>Join the Newsletter</h3>

                    <p>
                        Get the news about the latest releases and events, including subscriber exclusives.
                    </p>

                    <?php echo do_shortcode( '[mailchimp size=inline]' ); ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div id="footer">
            <div class="container">
                <footer class="row pt-4">
                    <nav id="footer-column-1" class="col-md-4">
                        <?php $streaming_items = wp_get_nav_menu_items( 'streaming-menu' ); ?>
                        <?php $social_items = wp_get_nav_menu_items( 'social-menu' ); ?>

                        <?php if ( !empty( $streaming_items ) || !empty( $social_items ) ): ?>
                        <h3>Connect</h3>
                        <?php endif; ?>

                        <?php if ( !empty( $streaming_items ) ): ?>
                            <ul id="nav-buy">
                                <?php foreach ( $streaming_items as $streaming_item ): ?>
                                    <li><a href="<?php echo esc_url( $streaming_item->url ) ?>"><span class="<?php echo implode( ' ', $streaming_item->classes ); ?>"></span> <span class="sr-only"><?php echo $streaming_item->post_title; ?></span></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <?php if ( !empty( $social_items ) ): ?>
                            <ul id="nav-social">
                                <?php foreach ( $social_items as $social_item ): ?>
                                    <li><a href="<?php echo esc_url( $social_item->url ) ?>"><span class="<?php echo implode( ' ', $social_item->classes ); ?>"></span> <span class="sr-only"><?php echo $social_item->post_title; ?></span></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </nav>

                    <section id="footer-column-2" class="col-md-4">
                        <?php $info_menu_args = array( 'theme_location' => 'footer-info', 'items_wrap' => '<ul id="%1$s" class="%2$s links">%3$s</ul>', 'echo' => false ); ?>
                        <?php if ( function_exists( 'bootstrap_page_menu' ) ) { $info_menu_args[ 'fallback_cb' ] = 'bootstrap_page_menu'; } ?>
                        <?php $more_info_menu = wp_nav_menu( $info_menu_args ); ?>

                        <?php if ( !empty( $more_info_menu ) ): ?>
                        <h3>See also ...</h3>

                        <?php echo $more_info_menu; ?>
                        <?php endif; ?>

                    </section>

                    <section id="footer-column-3" class="col-md-4">
                        <p>
                            <a href="https://observantrecords.com/">
                                <img class="footer-obrc-logo" src="/wp-content/themes/observantrecords2020/images/logo.png" title="[Observant Records]" alt="[Observant Records]" />
                            </a>
                        </p>

                        <p>
                            &copy <?php echo date('Y'); ?> Observant Records
                        </p>
                    </section>
                </footer>
            </div>
        </div>

	<?php wp_footer(); ?>

</body>
</html>