<?php
/**
 * @package WordPress
 * @subpackage ObservantRecords2020
 * @since Observant Records 2020 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;
?>
    <?php get_header(); ?>

    <div class="col-md-12">
        <?php if ( have_posts() ) : ?>
            <?php the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php the_content(); ?>
            </article>
        <?php endif; ?>
    </div>

    <?php get_sidebar( ); ?>
    <?php get_footer();
