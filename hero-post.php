<?php
namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;

use ObservantRecords\WordPress\Plugins\ArtistConnector\Models\Albums\Release;

$thumbnail = get_the_post_thumbnail_url();
if ( empty( $thumbnail ) && ( in_array( get_post_type(), array( 'post', 'page' ) )  ) ):
    $thumbnail = get_template_directory_uri() . '/images/blog-index-bg.jpg';
endif;


?>
<?php if ( !empty( $thumbnail ) ): ?>

<div class="jumbotron hero-header" style="background-image: url( '<?php echo $thumbnail; ?>' );">
    <div class="hero-overlay">
        <?php if ( is_single() || is_page() ): ?>
            <?php if ( 'post' == get_post_type() ): ?>
                <h5><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">Blog</a></h5>
            <?php endif; ?>
            <?php echo the_title('<h2 class="display-4 entry-title">', '</h2>'); ?>
        <?php else: ?>
            <?php echo the_title('<h3 class="display-4 entry-title"><a href="' . esc_url( get_permalink() )  . '" rel="bookmark">', '</a></h3>'); ?>
        <?php endif; ?>
        <div class="entry-meta">
            <ul class="list-inline">
                <?php if ( 'post' == get_post_type() ): ?>
                    <?php TemplateTags::posted_on(); ?>
                <?php endif; ?>

                <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
                    <li class="list-inline-item"><span class="fas fa-comment"></span> <?php comments_popup_link( __( 'Leave a comment', WP_TEXT_DOMAIN ), __( '1 Comment', WP_TEXT_DOMAIN ), __( '% Comments', WP_TEXT_DOMAIN ) ); ?></li>
                <?php endif; ?>

                <?php edit_post_link( __( 'Edit', WP_TEXT_DOMAIN ), '<li class="list-inline-item"><span class="fas fa-pencil-alt"></span> ', '</li>' ); ?>
            </ul>
        </div>
    </div>

</div>

<?php else: ?>
    <?php if ( is_single() || is_page() ): ?>
        <?php echo the_title('<h2 class="entry-title">', '</h2>'); ?>
    <?php else: ?>
        <?php echo the_title('<h3 class="entry-title"><a href="' . esc_url( get_permalink() )  . '" rel="bookmark">', '</a></h3>'); ?>
    <?php endif; ?>

    <div class="entry-meta">
        <ul class="list-inline">
            <?php if ( 'post' == get_post_type() ): ?>
                <?php TemplateTags::posted_on(); ?>
            <?php endif; ?>

            <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
                <li class="list-inline-item"><span class="fas fa-comment"></span> <?php comments_popup_link( __( 'Leave a comment', WP_TEXT_DOMAIN ), __( '1 Comment', WP_TEXT_DOMAIN ), __( '% Comments', WP_TEXT_DOMAIN ) ); ?></li>
            <?php endif; ?>

            <?php edit_post_link( __( 'Edit', WP_TEXT_DOMAIN ), '<li class="list-inline-item"><span class="fas fa-pencil-alt"></span> ', '</li>' ); ?>
        </ul>
    </div>

<?php endif; ?>
