<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 10/14/14
 * Time: 5:07 PM
 *
 * @package WordPress
 * @subpackage ObservantRecords2020
 * @since Musicwhore 2014 1.0
 */

namespace ObservantRecords\WordPress\Themes\ObservantRecords2020;
?>
<div class="col-md-4">
	<?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
	<?php dynamic_sidebar( 'sidebar-home' ); ?>
	<?php endif; ?>
</div>